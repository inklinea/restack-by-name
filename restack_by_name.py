#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Restack By Name - An Inkscape 1.2+ Extension
#  Restack objects or layers alphabetically according to label or ID
#  Appears in 'Extensions>Arrange
#  For shortcut triggered objects to new layer with last settings
# #############################################################################


import inkex

from inkex import Layer

# just get id
def get_id(x):
    return x.get_id()


# if element does not have a label, fallback to id
def get_label(x):

    label = x.get('inkscape:label')
    if not label:
        label = x.get_id()

    return label


# use id of layer first/last child to sort layer
def get_layer_child_id(x):
    layer_children = x.getchildren()

    if layer_children:
        object_id = layer_children[0].get_id()
    else:
        return None

    return object_id

# append objects to user chosen root
# by default we use reverse checkbox ticked in gui
# due to paint order of svg

def restack(self, object_list, reverse, group):

    if not reverse:
        [group.append(x) for x in object_list]
    else:
        [group.append(x) for x in reversed(object_list)]

# make a standard object list from an inkscape element list
# then sort that list according to criteria
def order_list(self, element_list):

    ordered_list = sorted(element_list, key=get_label)

    return ordered_list


class RestackByName(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass

        pars.add_argument("--decendant_type_radio", type=str, dest="decendant_type_radio", default='children')

        pars.add_argument("--group_destination_radio", type=str, dest="group_destination_radio", default='parent')

        pars.add_argument("--reverse_cb", type=inkex.Boolean, dest="reverse_cb", default=True)

    def effect(self):

        # Create empty list to contain all groups
        # Which we want to process
        master_group_list = []


        # Get the selection list from the Inkscape GUI
        # This also includes any selections in the Layers/Objects panel
        selection_list = self.svg.selected

        # Create a list of groups by filtering the selection list
        # For group tags ('g') this includes layers
        # which are a special type of group
        top_groups = [x for x in selection_list if x.TAG == 'g']


        # If we want to do the sorting on duplicates
        # in a new layer, duplicate the top_groups now
        if self.options.group_destination_radio == 'new_layer':
            new_layer = Layer()
            self.svg.append(new_layer)
            new_layer.set_random_id('New_Layer_')
            for top_group in top_groups:
                new_layer.append(top_group.duplicate())
            top_groups = new_layer.getchildren()

        # Exit if no groups are found
        if len(top_groups) < 1:
            inkex.errormsg('No Groups / Layers Selected')
            return

        # Append those initially filtered groups to the master list
        for item in top_groups:
            master_group_list.append(item)

        # Then add either child groups or all descendant groups
        # This will most likely create duplicate list entries
        # From groups within groups or cross group selections
        for top_group in top_groups:

            if self.options.decendant_type_radio == 'children':
                found_groups = [x for x in top_group if x.TAG == 'g']
                for item in found_groups:
                    master_group_list.append(item)

            else:
                found_groups = [x for x in top_group.descendants() if x.TAG == 'g']
                for item in found_groups:
                    master_group_list.append(item)


        # Sets cannot contain duplicates so list-->set-->list again.
        master_group_list = list(set(master_group_list))

        if len(master_group_list) < 1:
            inkex.errormsg('No Groups / Layers Found')
            return

        for group in master_group_list:

            group_children = group.getchildren()

            ordered_list = order_list(self, group_children)

            restack(self, ordered_list, self.options.reverse_cb, group)

if __name__ == '__main__':
    RestackByName().run()

